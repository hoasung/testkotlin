package com.s60.test.kotlin

import com.s60.test.kotlin.step01_getting_started.BasicSyntax
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
class ExampleUnitTest {
    @Test
    @Throws(Exception::class)
    fun addition_isCorrect() {
        assertEquals(4, (2 + 2).toLong())
    }

    @Test
    //Using collections
    fun testCollection(){
        val fruits = listOf("Apple", "Banana", "Orange")

        when{
            "Orange" in fruits -> println("Juice")
            "Apple" in fruits -> println("Apple is fine too")
        }
    }

    @Test
    fun testUsingStringTemplates(){
        var syntax =  BasicSyntax()

        val s = syntax.usingStringTemplates()

        println(s)
    }
}