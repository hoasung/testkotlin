package com.s60.test.kotlin.step03_classes_objects

/**
 * Created by thu.le on 5/26/2017.
 */

/*Classes and Interfaces
For members declared inside a class:
private means visible inside this class only (including all its members);
protected — same as private + visible in subclasses too;
internal — any client inside this module who sees the declaring class sees its internal members;
public — any client who sees the declaring class sees its public members.

NOTE for Java users: outer class does not see private members of its inner classes in Kotlin.

*/

/**
 * default is public
 */
fun publicFun() {}

public fun publicFun2() {}

/**
 * private, used only in this file
 */
private fun privateFun() {}

/**
 * internal: it is visible everywhere in the same module;
 */
internal fun internalFun() {}


/**
 * no protected in the top-level (file level)
 */
/*protected*/ fun protectedFun() {
}