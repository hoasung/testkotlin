//Package name: optional and it is not required to match with directories and packages (the difference with Java's package)
package com.s60.test.kotlin.step01_getting_started


//import

/**
 * Created by thu.le on 5/25/2017.
 */
class BasicSyntax {

    /*Comments: like Java*/

    //val: read-only, var: mutable
    fun defineLocalVariables() {

        //Read-only
        val a: Int = 1 // immediate assignment
        val b = 2 //'Int' type is inferred
        val c: Int //Type required when no initialized is provided
        c = 3

        //Mutable
        var x = 5
        x += 1

    }

    fun usingStringTemplates() :String {
        var a = 1
        val s1 = "a is $a"

        a = 2

        // arbitrary expression in template:
        val s2 = "${s1.replace("is", "was")}, but now is $a"

        return s2
    }

    //having two Int parameters with Int return type
    fun sum(a: Int, b: Int): Int {
        return a + b
    }


    //with an expression body and inferred returned type
    fun sub(a: Int, b: Int) = a - b


    //returning no meaningful value:
    fun printSum(a: Int, b: Int): Unit {
        println("sum of $a and $b is ${a + b}")
    }

    //Unit return type can be omitted
    fun printSub(a: Int, b: Int) {
        println("sub of $a and $b is ${a - b}")
    }

    //Using conditional expressions
    fun maxOf(a: Int, b: Int): Int {
        if (a > b) {
            return a
        } else {
            return b
        }
    }

    //Using if as an expression
    fun minOf(a: Int, b: Int) = if (a < b) a else b


    //Using if as an expression with last line used
    fun isPositive(a: Int) = if (a < 0) {
        println("$a is a positive number")
        true
    } else {
        println("$a is a negative number")
        false
    }

    //Using nullable values and checking for null
    fun parseInt(str: String): Int? {

        if (str != null) {
            return str.toIntOrNull()
        }

        return null
    }

    //Use a function returning nullable value
    fun printProduct(arg1: String, arg2: String) {
        val x = parseInt(arg1)
        val y = parseInt(arg2)

        if (x != null && y != null) { //if don't have this check line, compiler will show an error
            println(x * y)
        }
    }

    //Use a function returning nullable value
    fun orPrintProduct(arg1: String, arg2: String) {
        val x = parseInt(arg1)
        val y = parseInt(arg2)

        if (x == null) { //if don't have this check line, compiler will show an error
            return
        }

        if (y == null) { //if don't have this check line, compiler will show an error
            return
        }

        println(x * y)
    }

    //Using type checks and automatic casts
    fun getLength(obj: Any): Int? {
        if (obj is String) { //don't need check obj != null
            //obj is casted to String automatically
            return obj.length
        }

        if (obj is Map<*, *>) { //don't need check obj != null
            //obj is casted to Map automatically
            return obj.size
        }

        if (obj !is Array<*>) {
            return null
        }

        //obj is casted to Array automatically
        return obj.count()
    }

    //Using Loop
    fun testLoop() {
        val items = listOf("Apple", "Banana", "Kiwi")

        //Using a for loop
        for (item in items) {
            println(item)
        }

        //Using a while loop
        var index = 0
        while (index < items.size) {
            print(items[index])
            index++
        }
    }


    //Using when  expression
    fun describe(obj: Any): String =
            when (obj) {
                1 -> "One"
                2, 3 -> "Two or Three"
                in 5..10 -> "In range of 5 and 10"
                "Hello" -> "Greeting"
                is Long -> "Long type"
                !is String -> "Not a String type"
                else -> "Unknow"
            }

    //Using ranges 1
    fun checkRanges() {
        val x = 10
        val y = 9
        val z = -1

        //check in range
        if (x in 1..y + 1) {
            println("fits in range")
        }

        //check out range
        if (z !in 1..y + 2) {
            println("out of range")
        }
    }

    //Using range 2
    fun loopInRanges() {
        for (x in 1..3) {
            print(x) //123
        }
        println("...")

        for (x in 1..5 step 2) {
            print(x)//135
        }

        println("...")

        for (x in 1 until 3) {
            print(x)//12 (exclude 3)
        }

        println("...")

        for (x in 1 until 7 step 3) {
            print(x)//14 (exclude 7)
        }

        println("...")


        for (a in 3 downTo 1) {
            print(a)//321
        }

        println("...")

        for (a in 10 downTo 1 step 3) {
            print(a)//10741
        }

        println("...")
    }

    //Using collections
    fun testCollection() {
        val fruits = listOf("Apple", "Banana", "Box", "Ball", "Orange")

        //loop: see above


        //check condition
        when {
            "Orange" in fruits -> println("Juice is in the list")
            "Apple" in fruits -> println("Apple is fine too")
        }


        fruits.filter { it.startsWith("B") }
                .sortedBy { it.substring(1, 2) }
                .map { it.toUpperCase() }
                .forEach { println(it) }
    }

    //define functions 1
    fun test() {
        var total = sum(3, 4)

        var diff = sub(4, 3)

        print("Total:" + total)

        printSum(5, 6)
    }
}