package com.s60.test.kotlin.step03_classes_objects

/**
 * Created by thu.le on 5/26/2017.
 */

/**
 * Generic class
 */
class Box<T>(t: T) {
    var value = t
}

/**
 * Generic functions
 */
fun <T> singletonList(item: T): List<T> {
    return arrayListOf()
}

