package com.s60.test.kotlin.step02_basics

//define package is Optional


//Default Imports
//
//A number of packages are imported into every Kotlin file by default:
//
//kotlin.*
//kotlin.annotation.*
//kotlin.collections.*
//kotlin.comparisons.* (since 1.1)
//kotlin.io.*
//kotlin.ranges.*
//kotlin.sequences.*
//kotlin.text.*

class Step02_Packages {
    fun baz() {}
}
