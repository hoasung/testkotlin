package com.s60.test.kotlin

/**
 * Created by thu.le on 5/24/2017.
 */
data class Student(var age: Int, var name: String)