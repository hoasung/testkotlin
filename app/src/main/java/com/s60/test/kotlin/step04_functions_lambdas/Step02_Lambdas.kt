package com.s60.test.kotlin.step04_functions_lambdas

import com.s60.test.kotlin.step03_classes_objects.DataProviderManager.x
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.Lock

/**
 * Created by thu.le on 5/29/2017.
 */

/**
 * Higher-Order Functions:
 * A higher-order function is a function that takes functions as parameters, or returns a function
 */
fun <T> lock(lock: Lock, body: () -> T): T {
    lock.lock()
    try {
        return body()
    } finally {
        lock.unlock()
    }
}


/**
 * A lambda expression is always surrounded by curly braces,
 * Its parameters (if any) are declared before -> (parameter types may be omitted),
 * The body goes after -> (when present).
 */

var lock = Alock()

var f: () -> Int = fun(): Int { return 1 }

var f2: () -> Int = { 2 } //Lambda function

val resultOfLock = lock(lock, f) //pass a normal function
val resultOfLock2 = lock(lock, f2) //pass a normal function

val resultOfLock3 = lock(lock, { 3 }) //pass a Lambda function

/**
 * In Kotlin, there is a convention that if the last parameter to a function is a function
 */
val resultOfLock4 = lock(lock) { 4 }


fun <T, R> List<T>.map(transform: (T) -> R): List<R> {
    val result = arrayListOf<R>()
    for (item in this)
        result.add(transform(item))
    return result
}

var intsHighOrder = arrayOf(1, 2, 3)
/**
 * This function can be called as follows:
 */
val doubledHighOrdered = intsHighOrder.map { value -> value * 2 }


/**
 * it: implicit name of a single parameter
 * One other helpful convention is that if a function literal has only one parameter,
 * its declaration may be omitted (along with the ->), and its name will be it
 */
var itVar = intsHighOrder.map { it * 2 }


/**
 * Underscore for unused variables (since 1.1)
 */
val doubledHighOrderedUnderScore = intsHighOrder.map { _ -> 5 * 2 }


/**
 * Lambda Expression Syntax
 */
val sumLambda = { x: Int, y: Int -> x + y }
val sumLambda2: (Int, Int) -> Int = { x: Int, y: Int -> x + y }


/**
 * We can explicitly return a value from the lambda using the qualified return syntax.
 * Otherwise, the value of the last expression is implictly returned.
 * Therefore, the two following snippets are equivalent:
 */

var intsHighOrder2 = intsHighOrder.filter {
    val shouldFilter = it > 0
    shouldFilter
}

var intsHighOrder3 = intsHighOrder.filter {
    val shouldFilter = it > 0
    return@filter shouldFilter
}


/**
 * Anonymous Functions
 */

var aAnonymous = fun(x: Int, y: Int): Int = x + y //without return

var aAnonymous2 = fun(x: Int, y: Int): Int {
    return x + y //use the return. in the lambar, we must user the qualified syntax (see the above example: return@filter)
}


/**
 *
 * Function Literals with Receiver
 *
 */

var sum: Int.(otherValue: Int) -> Int = { otherValue -> this + otherValue }
var sum2 = fun Int.(otherValue: Int): Int = this + otherValue

var abc = 1.sum(2)
var abc2 = 1.sum2(2)

class HTML {
    fun body() {  }
}

fun html(init: HTML.() -> Unit): HTML {
    val html = HTML()  // create the receiver object
    html.init()        // pass the receiver object to the lambda
    return html
}

var aHtml = html {       // lambda with receiver begins here
    body()   // calling a method on the receiver object
}



/**
 *  a implement of Lock
 *
 */
class Alock : Lock {
    override fun lock() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun tryLock(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun tryLock(time: Long, unit: TimeUnit?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun unlock() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun lockInterruptibly() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun newCondition(): Condition {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

