package com.s60.test.kotlin.step03_classes_objects;
/**
 * Created by thu.le on 5/26/2017.
 */

import android.content.Context
import android.util.AttributeSet
import android.view.View

class Empty

/* default constructor  without any parameters */
class Step01_ClassAndInheritance {
}


/* a constructor  with  one parameter*/
class Person(firstName: String) {

    var firstname: String;

    init {
        //initialized blocks, the primary constructor can not store any code so we must do it in the init block for primary contractor
        this.firstname = firstName;
    }

}

/* Init fields by use the parameter
* */
class Customer(name: String) {
    val customerKey = name.toUpperCase()
}

/**
 * If the constructor has annotations or visibility modifiers, the constructor keyword is required, and the modifiers go before it
 */
class Student public /*@Inject*/ constructor(name: String) {
}

class BigPerson {

    /* Secondary Constructors */
    constructor(parent: Person) {
    }
}

/**
 * primary constructor  with a parameter
 */
class SmallPerson(val name: String) {

    /* Secondary Constructors call to the primary contractor */
    constructor(name: String, parent: Person) : this(name) {
    }
}


/**
 * default is public, but we need this is private
 */
class DontCreateMe private constructor() {
}

/**
 * Creating instances of classes
 *
 */

val bigPerson = BigPerson(Person("Thu"))

val customer = Customer("Joe Smith")


/**
 * Inheritance
 * Any is not java.lang.Object; in particular,
 * it does not have any members other than equals(), hashCode() and toString().
 */
class Example // Implicitly inherits from Any


/** To declare an explicit supertype, we place the type after a colon in the class header:
 *
 */
open class Base(p: Int)

class Derived(p: Int) : Base(p)

/**
 * If the class has no primary constructor, then each secondary constructor has to initialize the base type using the super keyword,
 * or to delegate to another constructor which does that.
 */
class MyView : View {
    constructor(ctx: Context) : super(ctx)
    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs)
}

/**
 * Overriding Methods
 */

open class Parent {
    open fun v() {}
    fun nv() {}
}

class Child : Parent() {
    override fun v() {}
    //override fun nv() {}: could not override nv, due to it is a final function
}

open class AnotherChild : Parent() {
    final override fun v() {} //v is final function in the AnotherChild, so any child of AnotherChild would be override this
}


/**
 * Overriding Properties
 */
open class Foo {
    open val x: Int = 1
}

class Bar1 : Foo() {
    override val x: Int = 2
}


interface IFoo {
    val count: Int
}

class Bar3(override val count: Int) : IFoo

class Bar4 : IFoo {
    override var count: Int = 0
}

/**
 * Overriding Rules
 */
open class A {
    open fun f() {
        print("A")
    }

    fun a() {
        print("a")
    }
}

interface B {
    fun f() {
        print("B")
    } // interface members are 'open' by default

    fun b() {
        print("b")
    }
}

class C() : A(), B {
    // The compiler requires f() to be overridden:
    override fun f() {
        super<A>.f() // call to A.f()
        super<B>.f() // call to B.f()
    }
}

/**
 * Abstract Classes
 */
open class NonAbstractBase {
    open fun f() {}
}

abstract class AbstractDerived : NonAbstractBase() {
    abstract override fun f()
    abstract fun a()
    fun b() {
    }
}

