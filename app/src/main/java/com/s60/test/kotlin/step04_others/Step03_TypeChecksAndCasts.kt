package com.s60.test.kotlin.step04_others

/**
 * Created by thu.le on 5/29/2017.
 */

/**
 * is and !is Operators
 *
 */
fun testType(obj: Any){
    if (obj is String) {
        //Smart Casts

        print(obj.length)
    }

    if (obj !is String) { // same as !(obj is String)
        print("Not a String")
    }
    else {

        //Smart Casts
        print(obj.length)
    }

    //"Unsafe" cast operator
    //the cast operator throws an exception if the cast is not possible. Thus, we call it unsafe.
    val x: String = obj as String

    //"Safe" (nullable) cast operator
    //To avoid an exception being thrown, one can use a safe cast operator as? that returns null on failure:
    val y: String? = obj as? String

    //Elvis Operator
    val l1: Int = if (y != null) y.length else -1
    //can chang to Elvis Operator like this:
    val l2 = y?.length ?: -1
    fun foo(node: String): String? {
        val parent = node?: return null
        val name = node?: throw IllegalArgumentException("name expected")

        return parent + name
    }
}


