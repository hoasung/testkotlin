package com.s60.test.kotlin.step04_others

import android.net.Network
import java.io.File

/**
 * Created by thu.le on 5/30/2017.
 */
typealias NodeSet = Set<Network>

typealias FileTable<K> = MutableMap<K, MutableList<File>>

typealias MyHandler = (Int, String, Any) -> Unit

typealias Predicate<T> = (T) -> Boolean


class A_Type {
    inner class Inner
}
class B_Type {
    inner class Inner
}

typealias AInner = A_Type.Inner
typealias BInner = B_Type.Inner

fun foo(p: Predicate<Int>) = p(42)

fun testTypeAlias(args: Array<String>) {
    val f: (Int) -> Boolean = { it > 0 }
    println(foo(f)) // prints "true"

    val p: Predicate<Int> = { it > 0 }
    println(listOf(1, -2).filter(p)) // prints "[1]"
}