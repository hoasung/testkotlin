package com.s60.test.kotlin.step04_others

import java.lang.Integer.parseInt

/**
 * Created by thu.le on 5/30/2017.
 */

fun exception(){
// To throw an exception object, use the throw-expression

    throw MyException("Hi There!")

// To catch an exception, use the try-expression

    try {
        // some code
    }
    catch (e: MyException) {
        // handler
    }
    finally {
        // optional finally block
    }
}


//Try is an expression
val a: Int? = try { parseInt("abc") } catch (e: NumberFormatException) { null }

//throw is an expression in Kotlin, so you can use it, for example, as part of an Elvis expression:
val s = a ?: throw IllegalArgumentException("Name required")


class MyException(s: String) : Throwable() {
}
