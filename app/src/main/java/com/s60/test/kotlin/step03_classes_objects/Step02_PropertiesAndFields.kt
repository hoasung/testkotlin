package com.s60.test.kotlin.step03_classes_objects

/**
 * Created by thu.le on 5/26/2017.
 */
class Step02_PropertiesAndFields {
}

class Address {
    /**
     * Classes in Kotlin can have properties. These can be declared as mutable, using the var keyword or read-only using the val keyword.
     */
    var name: String = ""
    var street: String = ""

    /**
     * use lateinit to init this var later
     */
    lateinit var represent: String

    /**
     * custom a getter
     */
    val isEmpty: Boolean
        get() = this.name == null || this.name.length <= 0


    /**
     * custom getter and setter
     */
    var stringRepresentation: String
        get() = if (this.represent != null) this.represent else this.toString()
        set(value) {
            this.represent = value
        }
}

/**
 * To use a property, we simply refer to it by name, as if it were a field in Java:
 */
fun copyAddress(address: Address): Address {
    val result = Address() // there's no 'new' keyword in Kotlin
    result.name = address.name // accessors are called
    result.street = address.street
    result.represent = address.represent
    return result
}
