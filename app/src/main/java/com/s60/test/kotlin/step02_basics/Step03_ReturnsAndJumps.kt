package com.s60.test.kotlin.step02_basics

/**
 * Created by thu.le on 5/25/2017.
 */
class Step03_ReturnsAndJumps {
//    Kotlin has three structural jump expressions:
//
//    return. By default returns from the nearest enclosing function or anonymous function.
//    break. Terminates the nearest enclosing loop.
//    continue. Proceeds to the next step of the nearest enclosing loop.

    data class Person(val name: String?)

    val person = Person("My Name")

    fun example() {
        val s = person.name ?: return
        println(s)
    }

    //Break and Continue Labels
    fun testBreak() {
        loop@ for (i in 1..100) {
            for (j in 1..100) {
                if (j * 2 == 8) break@loop
            }
        }
    }

    fun testReturnToMainFunction() {
        val ints = arrayOf(1, 2, 3, 0)
        ints.forEach {
            if (it == 0) return //will return out of testReturnToMainFunction function
            print(it)
        }

        println("will not be never called")
    }

    fun testReturnToLambdaExpression() {
        val ints = arrayOf(1, 2, 3, 0)

        ints.forEach lit@ {
            if (it == 0) return@lit //only return of out the forEach block, continue code of  testReturnToLambdaExpression
            print(it)
        }

        println("will be called")

    }

    fun testReturnToLambdaExpressionByItSelfLabel() {
        val ints = arrayOf(1, 2, 3, 0)

        ints.forEach {
            if (it == 0) return@forEach
            print(it)
        }
    }

    fun testReturnByUseAnonymousClearly() {
        val ints = arrayOf(1, 2, 3, 0)

        ints.forEach(fun(value: Int) {
            if (value == 0) return
            print(value)
        })
    }
}