package com.s60.test.kotlin.step03_classes_objects

/**
 * Created by thu.le on 5/26/2017.
 */
interface BaseDelegation {
    fun print()
}


class BaseBaseDelegationImpl(val x: Int) : BaseDelegation {
    override fun print() {
        print(x)
    }
}

/**
 * The by-clause in the supertype list for Derived indicates that b will be stored internally in objects of Derived
 * and the compiler will generate all the methods of Base that forward to b.
 */
class DerivedBaseDelegation(b: BaseDelegation, x: Int) : BaseDelegation by b

fun main(args: Array<String>) {
    val b = BaseBaseDelegationImpl(10)
    DerivedBaseDelegation(b, 3).print() // prints 10
}