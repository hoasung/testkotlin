package com.s60.test.kotlin

/**
 * Created by thu.le on 5/29/2017.
 */


/**
 * delegated-properties
 *
 * Need to research more
 *
 * http://kotlinlang.org/docs/reference/delegated-properties.html
 */


/**
 * Inline
 *
 * Need to research more
 *
 * http://kotlinlang.org/docs/reference/inline-functions.html
 *
 *  ??? need investigate more ???
 *
 * */


/**
 * Coroutines
 *
 * Need to research more
 *
 * http://kotlinlang.org/docs/reference/coroutines.html
 *
 * */


/**
 * Annotations
 *
 * Need to research more
 *
 * http://kotlinlang.org/docs/reference/annotations.html
 *
 * */

/**
 * Reflection
 * Need to research more
 * http://kotlinlang.org/docs/reference/reflection.html
 *
 * */

/**
 * Type-Safe Builders
 *
 * Need to research more
 *
 * http://kotlinlang.org/docs/reference/type-safe-builders.html
 *
 * */