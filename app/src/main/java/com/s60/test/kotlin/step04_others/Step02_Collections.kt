package com.s60.test.kotlin.step04_others

/**
 * Created by thu.le on 5/29/2017.
 */
val numbers: MutableList<Int> = mutableListOf(1, 2, 3)
val readOnlyView: List<Int> = numbers
val strings = hashSetOf("a", "b", "c", "c")
val items = listOf(1, 2, 3)
val readWriteMap = hashMapOf("foo" to 1, "bar" to 2)
val snapshot: Map<String, Int> = HashMap(readWriteMap)
