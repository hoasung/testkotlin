package com.s60.test.kotlin.step02_basics

/**
 * Created by thu.le on 5/25/2017.
 */
class Step01_BasicTypes {
//    Type	    Bit width
//    Double	64
//    Float	    32
//    Long	    64
//    Int	    32
//    Short	    16
//    Byte	    8

//    You can use underscores to make number constants more readable:

    val aint: Int = 123
    val along: Long = 123L
    val ahex = 0x0F //Default will be Int
    val ahexLong: Long = 0x0FL
    val abinary = 0b00001011
    val adouble = 123.5
    val afloat = 123.5f

    val oneMillion = 1_000_000 //default will be Int
    val creditCardNumber = 1234_5678_9012_3456L //default wil be Long
    val socialSecurityNumber = 999_99_9999L //default will be Long
    val hexBytes = 0xFF_EC_DE_5E //default will be Long
    val bytes = 0b11010010_01101001_10010100_10010010 //default will be Long

    //Booleans
    //The type Boolean represents booleans, and has two values: true and false.
    //          || – lazy disjunction
    //          && – lazy conjunction
    //          ! - negation
    var a = true
    var b = false
    val c = a || !b
    val d = b && c

    //Characters
    //Characters are represented by the type Char. They can not be treated directly as numbers
    fun decimalDigitValue(c: Char): Int {
        if (c !in '0'..'9')
            throw IllegalArgumentException("Out of range")
        return c.toInt() - '0'.toInt() // Explicit conversions to numbers
    }


    //Strings are represented by the type String

    fun printCharInString(str: String) {
        for (c in str) {
            println(c)
        }
    }

    val aString: String = "Hello, world!\n"

    //A raw string is delimited by a triple quote ("""), contains no escaping and can contain newlines and any other characters:
    val text1 = """
    for (c in "foo")
        print(c)
"""

    val text2 = """
    |Tell me and I forget.
    |Teach me and I remember.
    |Involve me and I learn.
    |(Benjamin Franklin)
    """.trimMargin()


    //String Templates
    val i = 10
    val s1 = "i = $i" // evaluates to "i = 10"

    val s = "abc"
    val str = "$s.length is ${s.length}" // evaluates to "abc.length is 3"

    //Arrays
    val numbers = arrayOf(1, 2, 3)
    // Creates an Array<String> with values ["0", "1", "4", "9", "16"]
    val asc = Array(5, { i -> (i * i).toString() })
    val sumOfFirstTwoItem = asc[1] + asc[2]


    fun test() {
        println(text2)
    }


}