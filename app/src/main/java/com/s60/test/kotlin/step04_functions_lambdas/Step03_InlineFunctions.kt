package com.s60.test.kotlin.step04_functions_lambdas

import java.util.concurrent.locks.Lock

/**
 * Created by thu.le on 5/29/2017.
 */


/**
 * http://kotlinlang.org/docs/reference/inline-functions.html
 *
 *  ??? need investigate more ???
 *
 * */

inline fun <T> lock2(lock: Lock, body: () -> T): T {
    return body()
}


inline fun foo(inlined: () -> Unit, noinline notInlined: () -> Unit) {
}

fun hasZeros(ints: List<Int>): Boolean {
    ints.forEach {
        if (it == 0) return true // returns from hasZeros
    }
    return false
}
