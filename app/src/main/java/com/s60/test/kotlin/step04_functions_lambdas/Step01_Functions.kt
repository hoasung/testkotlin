package com.s60.test.kotlin.step04_functions_lambdas

/**
 * Created by thu.le on 5/29/2017.
 */

/**
 * Functions in Kotlin are declared using the fun keyword
 * Parameters: Function parameters are defined using Pascal notation, i.e. name: type.
 * Parameters are separated using commas. Each parameter must be explicitly typed.
 */
fun multi(x: Int, y: Int): Int {
    return x * y
}


/**
 * Function Usage: Calling functions uses the traditional approach
 */
val result = multi(2, 3)


class Sample() {
    fun foo(): String {
        return "foo"
    }
}


/**
 * Calling member functions uses the dot notation
 */
var foo = Sample().foo() // create instance of class Sample and calls foo


/**
 * Infix notation
Functions can also be called using infix notations when
They are member functions or extension functions
They have a single parameter
They are marked with the infix keyword
 */
// Define extension to Int
infix fun Int.shl(x: Int): Int {
    return this / x
}

// call extension function using infix notation
var aashll = 1 shl 2

// is the same as
var bshl = 1.shl(2)


/**
 * Default Arguments
 */
fun read(b: Array<Byte>, off: Int = 0, len: Int = b.size) {
}

/**
 * call with default values
 */
var aread = read(arrayOf(0, 1, 2))


fun reformat(str: String,
             normalizeCase: Boolean = true,
             upperCaseFirstLetter: Boolean = true,
             divideByCamelHumps: Boolean = false,
             wordSeparator: Char = ' ') {
}

var areformat = reformat("a string", wordSeparator = '_')

/**
 * Single-Expression functions
 */
fun double(x: Int): Int = x * 2


/**
 * Variable number of arguments (Varargs)
 */
fun <T> asList(vararg ts: T): List<T> {
    val result = ArrayList<T>()
    for (t in ts) // ts is an Array
        result.add(t)
    return result
}


val list = asList(1, 2, 3)

val a = arrayOf(1, 2, 3)
val listWithArray = asList(-1, 0, *a, 4) //use  the spread operator (prefix the array with *):


/**
 * Local Functions: fun in another func
 * */
fun dfs(graph: String) {
    fun dfs(current: String, visited: Set<String>) {
    }
    dfs("abc", HashSet())
}
