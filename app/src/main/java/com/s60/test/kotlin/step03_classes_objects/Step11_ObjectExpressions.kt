package com.s60.test.kotlin.step03_classes_objects

import android.view.View

/**
 * Created by thu.le on 5/26/2017.
 */

/**
 * To create an object of an anonymous class that inherits from some type (or types), we write:
 */
fun testAnonymousClass(view: View) {
    view.setOnClickListener(object : View.OnClickListener {
        override fun onClick(v: View?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    })
}

open class Step11A(x: Int) {
    public open val y: Int = x
}

interface Step11B {}


/**
 * create a anonymous class variable from A & B above
 */
val anonymousVar: Step11A = object : Step11A(1), Step11B {
    override val y = 15
}

/**
 * If, by any chance, we need "just an object", with no nontrivial supertypes, we can simply say:
 */
fun foo() {
    val adHoc = object {
        var x: Int = 0
        var y: Int = 0
    }
    adHoc.x = 1
    print(adHoc.x + adHoc.y)
}

class Step11_C {
    // Private function, so the return type is the anonymous object type
    private fun foo() = object {
        val x: String = "x"
    }

    // Public function, so the return type is Any
    fun publicFoo() = object {
        public val x: String = "x"
    }

    fun bar() {
        val x1 = foo().x        // Works
        //val x2: Step11B = publicFoo().x  // ERROR: Unresolved reference 'x'
    }
}

/**
 * Object declarations
 * Singleton is a very useful pattern, and Kotlin (after Scala) makes it easy to declare singletons:
 */
object DataProviderManager: Step11B {
    var x = 1

    fun registerDataProvider(provider: String) {
    }

    val allDataProviders: Collection<String>
        get() = arrayListOf("a", "b")
}

fun testDataProviderManager() {
    DataProviderManager.registerDataProvider("contactProvider")
}


/**
 * Companion Objects
 */
class MyClass {
    /**
     * An object declaration inside a class can be marked with the companion keyword:
     */
    companion object Factory {
        fun create(): MyClass = MyClass()
    }
}

/**
 * Members of the companion object can be called by using simply the class name as the qualifier:
 */

fun tetCompanion(){
    val instance1 = MyClass.Factory.create() //like static function

    //The name of the companion object can be omitted, in which case the name Companion will be used:
    val instance2 = MyClass.create() //like static function
}
