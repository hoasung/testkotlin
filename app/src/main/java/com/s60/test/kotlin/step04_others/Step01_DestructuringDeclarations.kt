package com.s60.test.kotlin.step04_others

/**
 * Created by thu.le on 5/29/2017.
 */

class Person1(val name: String, val age: Int) {
    operator fun component1(): Any {
        return this.name
    }

    operator fun component2(): Any {
        return this.age
    }
}


data class Person2(val name: String, val age: Int);

fun destruct() {
    val (name, age) = Person1("Thu", 12)

    val (name2, age2) = Person2("Thu", 12)

}
