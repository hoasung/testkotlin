package com.s60.test.kotlin.step03_classes_objects

/**
 * Created by thu.le on 5/26/2017.
 */

class Outer {
    private val bar: Int = 1

    /*nested class*/
    class Nested {
        fun foo() = 2 //could not access bar variable
    }

    /**
     * Inner classes
     */
    inner class Inner {
        fun foo() = bar //can access bar variable
    }
}

fun testNestedClasses(){
    val demo1 = Outer.Nested().foo() // == 2
    val demo2 = Outer().Inner().foo() // == 1
}