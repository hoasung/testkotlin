package com.s60.test.kotlin.step03_classes_objects

/**
 * Created by thu.le on 5/26/2017.
 */

/**
 * Interfaces in Kotlin are very similar to Java 8
 */
interface MyInterface {
    fun bar()
    fun foo() {
        // optional body
    }
}

/**
 * Implementing Interfaces
 */
class Implementation : MyInterface {
    override fun bar() {
        // body
    }
}