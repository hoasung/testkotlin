package com.s60.test.kotlin.step03_classes_objects

/**
 * Created by thu.le on 5/26/2017.
 */

/**
 * We frequently create a class to do nothing but hold data.
 */
data class User(val name: String, var age: Int)

val a = User("Thu", 19)
val b = a.copy() //Copying

/*
* Data Classes and Destructuring Declarations
* */
fun test() {
    val jane = User("Jane", 35)
    jane.age = 20
    val (name, age) = jane   //<-----
    println("$name, $age years of age") // prints "Jane, 35 years of age"
}

