package com.s60.test.kotlin.step03_classes_objects

/**
 * Created by thu.le on 5/26/2017.
 */

/**
 * extend a class with new functionality without having to inherit from the class or use any type of design pattern such as Decorator.
 */
fun MutableList<Int>.swap1(index1: Int, index2: Int) {
    val tmp = this[index1] // 'this' corresponds to the list
    this[index1] = this[index2]
    this[index2] = tmp
}

fun testExtention() {
    val l = mutableListOf(1, 2, 3)
    l.swap1(0, 2) // 'this' inside 'swap()' will hold the value of 'l'
}

/**
 *  we can make it generic:
 */
fun <T> MutableList<T>.swap2(index1: Int, index2: Int) {
    val tmp = this[index1] // 'this' corresponds to the list
    this[index1] = this[index2]
    this[index2] = tmp
}


/**
 * Extension Properties
 */
val <T> List<T>.lastIndex: Int
    get() = size - 1
